<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vipinkumar
 * Date: 3/5/13
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Form;

use Zend\Form\Form;

class SignupForm extends Form
{

    protected $inputFilter;
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('login');

        $this->setAttribute('method', 'post')
            ->setAttribute('action', '/success');
        $this->add(array(
            'name' => 'firstName',
            'attributes' => array(
                'type'  => 'text',
                'class' =>'input-text'
            ),
            'options' => array(
                'label' => 'First Name',
            ),
        ));
        $this->add(array(
            'name' => 'lastName',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'input-text'
            ),
            'options' => array(
                'label' => 'Last Name',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'input-text'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'   => 'password',
                'class'  => 'input-text'
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Submit',
                'id' => 'submitbutton',
            ),
        ));

        $this->setInputFilter($this->getInputFilter());
    }
}