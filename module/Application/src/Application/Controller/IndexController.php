<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\SignupForm;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $form = new SignupForm();
    	/*echo("test1");
    	$SignUpForm = new Zend_Form();
    	echo("test2");
    	$SignUpForm->setAction('success');
    	echo("test3");
    	$SignUpForm->setMethod('post');
    	echo("test4");
    	$SignUpForm->setDescription('Sign Up Form');
    	echo("test5");
    	$SignUpForm->setAttrib('sitename', 'Penman Research');
    	echo("test6");

    	//Create Username field
    	$SignUpForm->addElement('text','username');
    	$usernameElement=$form->getElement('username');
    	$usernameElement->setLabel('Username:');

    	$this->view->form=$SignUpForm;*/
    	//return new ViewModel();
        return array('form' => $form);
        //return new ViewModel();
    }
}
